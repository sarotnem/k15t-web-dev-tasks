import React from "react";

function DatatableRow(props) {
  const { item } = props;
  console.log(item);
  return (
    <tr>
      <td>
        <strong>{item.username}</strong>
      </td>
      <td>
        <a
          href={`mailto:${item.email}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {item.email}
        </a>
      </td>
      <td>
        <a
          href={`https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(
            item.address.street
          )}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          {item.address.street}
        </a>
      </td>
      <td>{item.address.city}</td>
      <td>{item.company.name}</td>
      <td>{item.company.catchPhrase}</td>
    </tr>
  );
}

export default DatatableRow;
