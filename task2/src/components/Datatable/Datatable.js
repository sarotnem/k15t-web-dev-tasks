import React, { useState, useEffect } from "react";
import "./Datatable.scss";
import Table from "react-bootstrap/Table";
import DatatableRow from './DatatableRow'

function Datatable() {
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    getUsersFromApi();
  }, []);

  const getUsersFromApi = async () => {
    const url = "https://jsonplaceholder.typicode.com/users";

    const response = await fetch(url);
    const apiData = await response.json();
    setTableData(apiData);
  };

  return (
    <Table bordered hover striped>
      <thead className="text-center">
        <tr>
          <th>Username</th>
          <th>E-mail</th>
          <th>Street</th>
          <th>City</th>
          <th>Company</th>
          <th>Company catch phrase</th>
        </tr>
      </thead>
      <tbody>
        {tableData.map((item) => (
          <DatatableRow item={item} key={item.id}/>
        ))}
      </tbody>
    </Table>
  );
}

export default Datatable;
