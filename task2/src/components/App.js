import React from "react";
import "./App.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Datatable from './Datatable/Datatable';

function App() {
  return (
    <div className="App">
      <Container>
        <Row>
          <Col>
            <h1>Task 2: please create a table with the data from the API</h1>
          </Col>
        </Row>
        <div className="content mt-3">
          <Row>
            <Col>
              <Datatable id="datatable" />
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default App;
