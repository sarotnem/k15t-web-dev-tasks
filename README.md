# Web Developer Sample Project #

## What is it about?
Thank you so much for your interest in working at K15t and that you give us the chance to get an impression of your way of working. Besides the most important part of possible employees, the person itself, of cause we ware also interested how you solve problems and if that is a fit to our way too.
We know there are million ways to get to a certain goal so please feel free to use any technology or framework you need, know and love.

Please view these tasks as your possibility to showcase your skills. Let us know any assumptions you made for the tasks or ideas how they could be further improved.

## But *what* should I do exactly?
* Fork this repository
* Showcase your skills with the given tasks
* Check in your changes into your repository
* Write your contact person that you are done with the tasks and forward the link to your repository

#### Task 1
Update the css files and add responsive behavior to the design.
It should look nice for every screen resolution, at least for smartphones.

#### Task 2
Please call the JSON API: https://jsonplaceholder.typicode.com/users and display the data in a table.

###### The table should contain a column for:
* Username
* E-Mail (click should open e-mail client)
* Street (click should open google maps)
* City
* Company
* Company catch phrase

#### Task 3
The image provides how the final view should look like. Recreate the design with html and css in the given box.
You will find detailed design specification with sizes, colors etc. in the "task3/design_specs" folder.


## What's expected of me?
When our engineers receive your final result, we'll be looking at the following things:

* Improvements you made around the main task
* The quality and style of code written
* The choice of technologies used to complete the task. You are free to use what every you think is needed and helps you to get it done!


Good luck!
